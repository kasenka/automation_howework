from connection import Connection
from products_class import Product


connection = Connection()


cursor = connection.cursor()

update_query = (
    "update products as p "
    "set price = 1000000 "
    "from orders as o "
    "where o.quantity < 10 "
    "and p.id = o.product_id;"
)

cursor.execute(update_query)

print('Results after updating products:')
cursor.execute("select * from products;")

result_after_update = cursor.fetch_all()

print("-" * 50)
products = Product.from_cursor_data(result_after_update)

for product in products:
    print(product)

connection.commit()