from connection import Connection
from products_class import Product


connection = Connection()

cursor = connection.cursor()

print('Results before adding products:')
cursor.execute("select * from products;")

result_before_insert = cursor.fetch_all()

products = Product.from_cursor_data(result_before_insert)

for product in products:
    print(product)

info_products = ({"id": "nnnnn", "name": 'Meizu 5', "price": 500},
                 {"id": 'kkkkk', "name": 'Meizu 6', "price": 600},
                 {"id": 'lllll',  "name": 'Meizu 7', "price": 700},
                 {"id": 'mmmmm',  "name": 'Meizu 8', "price": 800})

cursor.execute_many("""INSERT INTO products (id, name, price) VALUES (%(id)s, %(name)s, %(price)s)""", info_products)


print('Results after adding products:')
cursor.execute("select * from products;")

result_after_insert = cursor.fetch_all()

print("-" * 50)
products = Product.from_cursor_data(result_after_insert)

for product in products:
    print(product)



info_orders = ({"id": "nnnnn", "quantity": 2, "product_id": "nnnnn"},
              {"id": 'kkkkk', "quantity": 5, "product_id": 'kkkkk'},
              {"id": 'lllll',  "quantity": 8, "product_id": 'lllll'},
              {"id": 'mmmmm',  "quantity": 9, "product_id": 'mmmmm'})

cursor.execute_many("""INSERT INTO orders (id, quantity, product_id) VALUES (%(id)s, %(quantity)s, %(product_id)s)""", info_orders)


print('Results after adding orders:')
cursor.execute("select * from orders;")

result_after_insert_orders = cursor.fetch_all()

print("-" * 50)
orders = Product.from_cursor_data(result_after_insert_orders)

for order in orders:
    print(order)

connection.commit()
