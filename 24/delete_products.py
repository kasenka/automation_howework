from connection import Connection
from products_class import Product


connection = Connection()

cursor = connection.cursor()

delete_query_1 = (
    "delete  "
    "from orders o "
    "using products p "
    "where o.quantity > 10 "
    "and p.id = o.product_id;"
)
print('Results before delete from orders:')

cursor.execute("select * from orders;")
result_before_delete_1 = cursor.fetch_all()
print("-" * 50)
orders = Product.from_cursor_data(result_before_delete_1)

for order in orders:
    print(order)

cursor.execute(delete_query_1)

print('Results after delete from orders:')
cursor.execute("select * from orders;")

result_after_delete_1 = cursor.fetch_all()
print("-" * 50)
orders = Product.from_cursor_data(result_after_delete_1)

for order in orders:
    print(order)


delete_query_2 = (
    "delete  "
    "from products  "
    "where id = 'aaaaa'; "
)

print('Results before delete from products:')
cursor.execute("select * from products;")
result_before_delete_2 = cursor.fetch_all()
print("-" * 50)
products = Product.from_cursor_data(result_before_delete_2)

for product in products:
    print(product)

cursor.execute(delete_query_2)


print('Results after delete from products:')
cursor.execute("select * from products;")

result_after_delete_2 = cursor.fetch_all()

print("-" * 50)
products = Product.from_cursor_data(result_after_delete_2)

for product in products:
    print(product)

connection.commit()