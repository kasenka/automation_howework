CREATE TABLE orders(
    id VARCHAR(5) PRIMARY KEY,
    quantity INT not null,
    product_id VARCHAR(5),
    CONSTRAINT product_id
        FOREIGN KEY (id)
        REFERENCES products(id)
);